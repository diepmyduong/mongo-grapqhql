import DataLoader from 'dataloader';
import { {Name}Model, {Name}Doc } from './{name}.model';

export const {Name}Loader = new DataLoader<string, {Name}Doc>((ids: string[]) => {
  return {Name}Model.find({ _id: { $in: ids } }).exec();
});
