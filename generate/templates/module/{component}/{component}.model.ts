import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { MainConnection } from '../../../config/connections/main';
import { BaseDocument } from '../model';

const Schema = mongoose.Schema;

export type {Name}Doc = BaseDocument & {
  
};

const {name}Schema = new Schema(
  {
    
  },
  { timestamps: true }
);

{name}Schema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let {Name}Model: mongoose.Model<{Name}Doc> = MainConnection.model(
  '{Name}',
  {name}Schema
);
