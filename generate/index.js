const inquirer = require('inquirer');
const generator = require('custom-template-generator');
const fs = require('fs');
const _ = require('lodash');
const DIR = process.env['PWD'];
const path = require('path');
const options = [
    { id: 'helper', action: createHelper },
    { id: 'module', action: createModule }
]
const questions = [{
    type: 'list',
    name: 'template',
    message: 'Generate Option?',
    choices: options.map(o => o.id)
}]

async function run() {
    const { template } = await inquirer.prompt(questions);
    const option = _.find(options, o => o.id == template);
    if (!option) return;
    option.action();
}



async function createHelper() {
    const { name } = await inquirer.prompt([
        { type: 'input', name: 'name', message: 'helper name?' }
    ]);

    generator({
        componentName: _.camelCase(name),
        customTemplatesUrl: './generate/templates',// path.join(DIR,'generate/templates'),
        dest: `./src/helper`, // path.join(DIR,'src',version),
        templateName: "helper",
        wrapInFolder: false,
        data: { name: _.camelCase(name) }
    });
}
async function createModule() {
    const { name } = await inquirer.prompt([
        { type: 'input', name: 'name', message: 'module name?' }
    ]);

    generator({
        componentName: _.camelCase(name),
        customTemplatesUrl: './generate/templates',// path.join(DIR,'generate/templates'),
        dest: `./src/graphql/modules`, // path.join(DIR,'src',version),
        templateName: "module",
        wrapInFolder: false,
        data: { name: _.camelCase(name) }
    });
}
run();