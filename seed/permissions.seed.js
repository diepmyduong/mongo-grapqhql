const { PermissionModel } = require('../dev/graphql/modules/permission/permission.model');

const PermissionSeed = async function() {

    const results = await PermissionModel.insertMany([
        { name: 'read', desc: 'Xem dữ liệu', readOnly: true },
        { name: 'write', desc: 'Ghi dữ liệu', readOnly: true }
    ], { ordered: false }).catch(err => {});
    console.log('inserted', results);
}

module.exports = PermissionSeed;