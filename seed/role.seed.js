const { RoleModel } = require('../dev/graphql/modules/role/role.model');

const RoleSeed = async function() {

    const results = await RoleModel.insertMany([
        { name: 'admin', desc: 'Quản trị viên', readOnly: true },
        { name: 'editor', desc: 'Biên tập viên', readOnly: true },
        { name: 'customer', desc: 'Khách hàng', readOnly: true },
    ], { ordered: false }).catch(err => {});;
    console.log('inserted', results);
}

module.exports = RoleSeed;