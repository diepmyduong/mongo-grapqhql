const { UserModel } = require('../dev/graphql/modules/user/user.model');
const bcrypt = require('bcryptjs');

const UserSeed = async function() {
    const Admin = new UserModel({
        username: 'admin',
        password: await bcrypt.hash(`admin`, 12),
        role: 'admin',
        permissions: []
    })

    const Editor = new UserModel({
        username: 'editor',
        password: await bcrypt.hash(`editor`, 12),
        role: 'editor',
        permissions: []
    })

    const Customer = new UserModel({
        username: 'customer',
        password: await bcrypt.hash(`customer`, 12),
        role: 'customer',
        permissions: []
    })

    const results = await UserModel.insertMany([Admin, Customer, Editor], { ordered: false }).catch(err => {});;
    console.log('inserted', results);
}

module.exports = UserSeed;