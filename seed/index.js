const { UtilHelper } = require('../dev/helper/util/util.helper');

const SeedFiles = UtilHelper.walkSyncFiles(__dirname)
    .filter(f => /(.*).seed.js$/.test(f)).map(f => {
    const seed = require(f);
    try {
        if (seed) seed();
    } catch (err) {
        console.log('Seed Error', err.message);
    }
})

