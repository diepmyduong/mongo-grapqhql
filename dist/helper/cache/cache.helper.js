"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
const node_cache_1 = __importDefault(require("node-cache"));
const buckets = [];
class CacheHelper {
    static getBucket(bucketName, option = {}) {
        let bucket = buckets.find(b => b.bucketName == bucketName);
        if (!bucket) {
            const cache = new node_cache_1.default(lodash_1.default.merge({ stdTTL: 300, checkperiod: 120, useClones: false }, option));
            bucket = { bucketName, cache };
            buckets.push(bucket);
        }
        return bucket;
    }
    static set(key, value, bucketName = 'default') {
        const bucket = CacheHelper.getBucket(bucketName);
        bucket.cache.set(key, value);
    }
    static get(key, bucketName = 'default') {
        const bucket = CacheHelper.getBucket(bucketName);
        return bucket.cache.get(key);
    }
    static clear(bucketName) {
        const bucket = CacheHelper.getBucket(bucketName);
        bucket.cache.flushAll();
    }
}
exports.CacheHelper = CacheHelper;
