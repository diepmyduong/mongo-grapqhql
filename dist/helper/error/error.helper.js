"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const baseError_1 = require("./baseError");
class ErrorHelper extends baseError_1.ErrorHelper {
    static userNotExist() {
        return new baseError_1.BaseError(403, '-103', 'Người dùng khồng tồn tại');
    }
    static userExisted() {
        return new baseError_1.BaseError(403, '-104', 'Người dùng đã tồn tại');
    }
    static userRoleNotSupported() {
        return new baseError_1.BaseError(401, '-105', 'Người dùng không được cấp quyền');
    }
    static userError(message) {
        return new baseError_1.BaseError(403, '-106', 'Lỗi người dùng: ' + message);
    }
    static duplicateError(key) {
        return new baseError_1.BaseError(403, '-107', `${key} đã bị trùng.`);
    }
    static readOnlyError(key) {
        return new baseError_1.BaseError(403, '-108', `${key} chỉ được phép xem.`);
    }
}
exports.ErrorHelper = ErrorHelper;
