"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BaseError extends Error {
    constructor(status, code, message, data) {
        super(message);
        this.info = { status, code, message, data };
    }
}
exports.BaseError = BaseError;
class ErrorHelper {
    static handleError(func) {
        return (req, res) => func
            .bind(this)(req, res)
            .catch((error) => {
            if (!error.info) {
                console.log('UNKNOW ERROR', error);
                const err = ErrorHelper.somethingWentWrong();
                res.status(err.info.status).json(err.info);
            }
            else {
                res.status(error.info.status).json(error.info);
            }
        });
    }
    static logError(prefix, logOption = true) {
        return (error) => {
            console.log(prefix, error.message || error, logOption ? error.options : '');
        };
    }
    static somethingWentWrong(message) {
        return new BaseError(500, '500', message || 'Có lỗi xảy ra');
    }
    static unauthorized() {
        return new BaseError(401, '401', 'Chưa xác thực');
    }
    static badToken() {
        return new BaseError(401, '-1', 'Không có quyền truy cập');
    }
    static tokenExpired() {
        return new BaseError(401, '-2', 'Mã truy cập đã hết hạn');
    }
    static permissionDeny() {
        return new BaseError(405, '-3', 'Không đủ quyền để truy cập');
    }
    static requestDataInvalid(message) {
        return new BaseError(403, '-4', 'Dữ liệu gửi lên không hợp lệ');
    }
    static externalRequestFailed(message) {
        return new BaseError(500, '-5', 'Có lỗi xảy ra');
    }
    static mgRecoredNotFound() {
        return new BaseError(404, '-7', 'Không tìm thấy dữ liệu yêu cầu');
    }
    static mgQueryFailed(message) {
        return new BaseError(403, '-8', message || 'Truy vấn không thành công');
    }
}
exports.ErrorHelper = ErrorHelper;
