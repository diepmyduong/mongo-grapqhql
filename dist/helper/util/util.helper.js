"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ajv_1 = __importDefault(require("ajv"));
const ajv_errors_1 = __importDefault(require("ajv-errors"));
const ajv_keywords_1 = __importDefault(require("ajv-keywords"));
const crypto_1 = __importDefault(require("crypto"));
const fs_1 = __importDefault(require("fs"));
const lodash_1 = __importDefault(require("lodash"));
const path_1 = __importDefault(require("path"));
class UtilHelper {
    static validateJSON(schema, json = {}) {
        const ajv = new ajv_1.default({ allErrors: true, jsonPointers: true });
        ajv_errors_1.default(ajv, { singleError: true });
        ajv_keywords_1.default(ajv, ['switch']);
        const valid = ajv.validate(schema, json);
        return {
            isValid: valid,
            message: ajv.errorsText()
        };
    }
    static booleanString(condition) {
        return condition ? 'True' : 'False';
    }
    static hash(data) {
        let s = data;
        if (!lodash_1.default.isString(data)) {
            s = JSON.stringify(data);
        }
        return crypto_1.default
            .createHash('md5')
            .update(s)
            .digest('hex');
    }
    static walkSyncFiles(dir, filelist = []) {
        const files = fs_1.default.readdirSync(dir);
        files.forEach(function (file) {
            if (fs_1.default.statSync(path_1.default.join(dir, file)).isDirectory()) {
                filelist = UtilHelper.walkSyncFiles(path_1.default.join(dir, file), filelist);
            }
            else {
                filelist.push(path_1.default.join(dir, file));
            }
        });
        return filelist;
    }
    static compactObject(obj) {
        return lodash_1.default.omitBy(obj, lodash_1.default.isUndefined);
    }
}
exports.UtilHelper = UtilHelper;
