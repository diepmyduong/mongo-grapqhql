"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_simple_1 = __importDefault(require("jwt-simple"));
const moment_1 = __importDefault(require("moment"));
const config_1 = require("../../config");
const error_helper_1 = require("../error/error.helper");
class TokenHelper {
    static generateToken(payload, option = {}) {
        const secret = option.secret || config_1.config.secret;
        return jwt_simple_1.default.encode({
            payload: payload,
            exp: option.exp || moment_1.default().add(1, 'days')
        }, secret);
    }
    static decodeToken(token, option = {}) {
        let result = undefined;
        try {
            const secret = option.secret || config_1.config.secret;
            result = jwt_simple_1.default.decode(token, secret);
        }
        catch (err) {
            throw error_helper_1.ErrorHelper.badToken();
        }
        if (result) {
            if (new Date(result.exp).getTime() <= Date.now()) {
                throw error_helper_1.ErrorHelper.tokenExpired();
            }
            return result.payload;
        }
        else {
            throw error_helper_1.ErrorHelper.badToken();
        }
    }
}
exports.TokenHelper = TokenHelper;
