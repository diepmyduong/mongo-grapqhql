"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const lodash_1 = __importDefault(require("lodash"));
const config_1 = require("../../config");
const error_helper_1 = require("../error/error.helper");
exports.FirebaseApp = firebase_admin_1.default.initializeApp({
    credential: firebase_admin_1.default.credential.cert(config_1.config.firebaseCredential),
    databaseURL: `https://${config_1.config.firebaseCredential.project_id}.firebaseio.com`,
    storageBucket: `${config_1.config.firebaseCredential.project_id}.appspot.com`
}, 'v1');
let realtimeConfig = {};
if (config_1.config.enableRealtimeConfig) {
    exports.FirebaseApp.database()
        .ref('system')
        .on('value', snap => {
        if (snap) {
            realtimeConfig = snap.val();
            console.log('update system config', realtimeConfig);
        }
    });
}
exports.FirebaseApp.database().ref('system');
class FirebaseHelper {
    static getConfig(path) {
        if (path) {
            return lodash_1.default.get(realtimeConfig, path);
        }
        else {
            return realtimeConfig;
        }
    }
    static verifyIdToken(token) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield exports.FirebaseApp.auth().verifyIdToken(token);
            }
            catch (err) {
                console.log('err', err);
                throw error_helper_1.ErrorHelper.badToken();
            }
        });
    }
    static createUser(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.auth().createUser({ email, password });
        });
    }
    static removeUser(uid) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.auth().deleteUser(uid);
        });
    }
    static getUserByEmail(email) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.auth().getUserByEmail(email);
        });
    }
    static getUserByPhone(phone) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.auth().getUserByPhoneNumber(phone);
        });
    }
    static getUserById(uid) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.auth().getUser(uid);
        });
    }
    static uploadFile(path, bucket) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.storage()
                .bucket(bucket)
                .upload(path);
        });
    }
    static getFile(filename, bucket) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.storage()
                .bucket(bucket)
                .file(filename)
                .download();
        });
    }
    static deleteFile(filename, bucket) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield exports.FirebaseApp.storage()
                .bucket(bucket)
                .file(filename)
                .delete();
        });
    }
}
exports.FirebaseHelper = FirebaseHelper;
