"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cluster_1 = __importDefault(require("cluster"));
const os_1 = __importDefault(require("os"));
const cpuCount = os_1.default.cpus().length;
const webWorkers = [];
if (cluster_1.default.isMaster) {
    for (let i = 0; i < cpuCount; i += 1) {
        addWebWorker();
    }
    cluster_1.default.on('exit', (worker, code, signal) => {
        if (webWorkers.indexOf(worker.id) != -1) {
            console.log('http worker ' + worker.process.pid + ' died. Trying to respawn...');
            removeWebWorker(worker.id.toString());
            addWebWorker();
        }
    });
}
else {
    if (process.env.web) {
        console.log('start http server: ' + cluster_1.default.worker.id);
        require('./server');
    }
}
function addWebWorker() {
    webWorkers.push(cluster_1.default.fork({ web: 1 }).id);
}
function removeWebWorker(id) {
    webWorkers.splice(webWorkers.indexOf(id), 1);
}
