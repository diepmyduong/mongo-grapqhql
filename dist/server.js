"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const body_parser_1 = __importDefault(require("body-parser"));
const compression_1 = __importDefault(require("compression"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const express_1 = __importDefault(require("express"));
const fs_1 = __importDefault(require("fs"));
const morgan_1 = __importDefault(require("morgan"));
const path_1 = __importDefault(require("path"));
const config_1 = require("./config");
const graphql_1 = __importDefault(require("./graphql"));
console.log('Starting server with at ' + process.pid + ' on port ' + config_1.config.port);
const app = express_1.default();
app.use(morgan_1.default('common', {
    skip: function (req, res) {
        if (req.url == '/_ah/health') {
            return true;
        }
        else {
            return false;
        }
    }
}));
app.use(body_parser_1.default.json({
    limit: '50mb'
}));
app.use(body_parser_1.default.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.set('view engine', 'ejs');
app.use(compression_1.default());
app.use(express_1.default.static(path_1.default.join(__dirname, './public')));
app.set('views', path_1.default.join(__dirname, './templates'));
app.use(cookie_parser_1.default());
app.set('port', config_1.config.port);
app.get('/api/version', function (request, response) {
    const version = JSON.parse(fs_1.default.readFileSync(path_1.default.resolve(__dirname, 'public/version.json'), 'utf8'));
    response.send(`${config_1.config.name}: ${version.echo}`);
    response.end();
});
graphql_1.default(app);
app.listen(app.get('port'), function () {
    console.log(`${config_1.config.name} started at ${config_1.config.domain}`);
});
