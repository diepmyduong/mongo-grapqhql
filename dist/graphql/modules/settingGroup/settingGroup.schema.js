"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const schema = apollo_server_express_1.gql `
    extend type Query {
        settingGroups: [SettingGroup]
        settingGroup(name: String!): SettingGroup
    }

    extend type Mutation {
        createSettingGroup(name: String!, desc: String): SettingGroup
        updateSettingGroup(_id: ID!, name: String, desc: String): SettingGroup
        deleteSettingGroups(_ids: [ID!]): Mixed
    }

    type SettingGroup {
        _id: ID
        name: String
        desc: String
        readOnly: Boolean
        settings: [Setting]
    }
`;
exports.default = schema;
