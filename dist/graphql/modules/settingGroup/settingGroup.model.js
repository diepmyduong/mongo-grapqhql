"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dataloader_1 = __importDefault(require("dataloader"));
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_hidden_1 = __importDefault(require("mongoose-hidden"));
const main_1 = require("../../../config/connections/main");
const Schema = mongoose_1.default.Schema;
const settingGroupSchema = new Schema({
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false },
    settings: { type: [Schema.Types.ObjectId], default: [] }
}, { timestamps: true });
settingGroupSchema.index({ name: 1 }, { unique: true });
settingGroupSchema.plugin(mongoose_hidden_1.default({ defaultHidden: { _id: false } }));
exports.SettingGroupModel = main_1.MainConnection.model('SettingGroup', settingGroupSchema);
exports.SettingGroupLoader = new dataloader_1.default((ids) => {
    return exports.SettingGroupModel.find({ _id: { $in: ids } }).exec();
});
