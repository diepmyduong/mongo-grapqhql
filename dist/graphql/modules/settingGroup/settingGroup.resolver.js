"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const error_helper_1 = require("../../../helper/error/error.helper");
const auth_helper_1 = require("../../helper/auth.helper");
const setting_model_1 = require("../setting/setting.model");
const role_1 = require("../user/role");
const settingGroup_model_1 = require("./settingGroup.model");
const Query = {
    settingGroups: (root, args, context) => {
        return settingGroup_model_1.SettingGroupModel.find()
            .lean(true)
            .exec();
    },
    settingGroup: (root, args, context) => {
        return settingGroup_model_1.SettingGroupModel.findOne({ name: args.name })
            .lean(true)
            .exec();
    }
};
const Mutation = {
    createSettingGroup: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { name, desc } = args;
        const fetchedGroup = yield settingGroup_model_1.SettingGroupModel.findOne({ name });
        if (fetchedGroup)
            throw error_helper_1.ErrorHelper.duplicateError(name);
        const newGroup = yield settingGroup_model_1.SettingGroupModel.create({ name, desc });
        settingGroup_model_1.SettingGroupLoader.prime(newGroup.id, newGroup);
        return newGroup;
    }),
    updateSettingGroup: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { _id, name, desc } = args;
        const fetchedGroup = yield settingGroup_model_1.SettingGroupLoader.load(_id);
        if (!fetchedGroup)
            throw error_helper_1.ErrorHelper.mgRecoredNotFound();
        if (fetchedGroup.readOnly)
            throw error_helper_1.ErrorHelper.readOnlyError(fetchedGroup.name);
        yield fetchedGroup
            .set('name', name)
            .set('desc', desc)
            .save();
        return fetchedGroup;
    }),
    deleteSettingGroups: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const removedGroups = yield settingGroup_model_1.SettingGroupModel.deleteMany({
            _id: { $in: args._ids },
            readOnly: false
        });
        settingGroup_model_1.SettingGroupLoader.clearAll();
        return removedGroups;
    })
};
const SettingGroup = {
    settings: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        return setting_model_1.SettingLoader.loadMany(root.settings).then(settings => {
            return settings.filter(s => {
                return (!s.isPrivate || auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN, false));
            });
        });
    })
};
exports.default = {
    Query,
    Mutation,
    SettingGroup
};
