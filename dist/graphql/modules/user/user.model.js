"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dataloader_1 = __importDefault(require("dataloader"));
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_hidden_1 = __importDefault(require("mongoose-hidden"));
const main_1 = require("../../../config/connections/main");
const Schema = mongoose_1.default.Schema;
const userSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true, hideJSON: true },
    role: { type: String, required: true },
    permissions: { type: [String], default: [] }
}, { timestamps: true });
userSchema.index({ username: 1 }, { unique: true });
userSchema.plugin(mongoose_hidden_1.default({ defaultHidden: { _id: false } }));
exports.UserModel = main_1.MainConnection.model('User', userSchema);
exports.UserLoader = new dataloader_1.default((ids) => {
    return exports.UserModel.find({ _id: { $in: ids } }).exec();
});
