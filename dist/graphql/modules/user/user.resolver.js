"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const error_helper_1 = require("../../../helper/error/error.helper");
const user_model_1 = require("./user.model");
const token_helper_1 = require("../../../helper/token/token.helper");
const auth_helper_1 = require("../../helper/auth.helper");
const role_1 = require("./role");
const query_helper_1 = require("../../helper/query.helper");
const Query = {
    users: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN_EDITOR);
        const query = user_model_1.UserModel.find().lean(true);
        return query_helper_1.applyQueryInput(args.q, query);
    })
};
const Mutation = {
    createUser: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { username, password, role, permissions } = args;
        if (!role_1.ROLES_ADMIN_EDITOR_CUSTOMER.includes(role)) {
            throw error_helper_1.ErrorHelper.userError(`Vai trò không hợp lệ. Chỉ hổ trợ ${role_1.ROLES_ADMIN_EDITOR_CUSTOMER.join(',')}`);
        }
        const fetchedUser = yield user_model_1.UserModel.findOne({ username });
        if (fetchedUser)
            throw error_helper_1.ErrorHelper.userExisted();
        const newUser = yield user_model_1.UserModel.create({
            username: username,
            password: yield bcryptjs_1.default.hash(password, 12),
            role: role,
            permissions
        });
        return newUser.toJSON();
    }),
    login: (root, args) => __awaiter(void 0, void 0, void 0, function* () {
        const { username, password } = args;
        const user = yield user_model_1.UserModel.findOne({ username });
        if (!user) {
            throw error_helper_1.ErrorHelper.userNotExist();
        }
        const isEqual = yield bcryptjs_1.default.compare(password, user.password);
        if (!isEqual) {
            throw new Error('Password is incorrect!');
        }
        const token = token_helper_1.TokenHelper.generateToken({
            userId: user._id,
            username: user.username,
            role: user.role,
            permissions: user.permissions
        });
        return { token, user: user.toJSON() };
    })
};
exports.default = {
    Query,
    Mutation
};
