"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const schema = apollo_server_express_1.gql `
    extend type Mutation {
        createUser(username: String!, password: String!, role: String!, permissions: [String]): User!
        login(username: String!, password: String!): AuthData!
    }
    
    extend type Query {
        users(q: QueryInput): [User]
    }

    type AuthData {
        user: User!
        token: String!
    }

    type User {
        _id: ID
        username: String
        role: String
        permissions: [String]
    }
`;
exports.default = schema;
