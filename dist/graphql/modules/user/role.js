"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROLE_ADMIN = 'admin';
exports.ROLE_EDITOR = 'editor';
exports.ROLE_CUSTOMER = 'customer';
exports.ROLES_ADMIN = ['admin'];
exports.ROLES_ADMIN_EDITOR = ['admin', 'editor'];
exports.ROLES_ADMIN_EDITOR_CUSTOMER = ['admin', 'editor', 'customer'];
