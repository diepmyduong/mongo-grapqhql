"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dataloader_1 = __importDefault(require("dataloader"));
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_hidden_1 = __importDefault(require("mongoose-hidden"));
const main_1 = require("../../../config/connections/main");
const Schema = mongoose_1.default.Schema;
const permissionSchema = new Schema({
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false }
}, { timestamps: true });
permissionSchema.index({ name: 1 }, { unique: true });
permissionSchema.plugin(mongoose_hidden_1.default({ defaultHidden: { _id: false } }));
exports.PermissionModel = main_1.MainConnection.model('Permission', permissionSchema);
exports.PermissionLoader = new dataloader_1.default((ids) => {
    return exports.PermissionModel.find({ _id: { $in: ids } }).exec();
});
