"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const error_helper_1 = require("../../../helper/error/error.helper");
const auth_helper_1 = require("../../helper/auth.helper");
const role_1 = require("../user/role");
const permission_model_1 = require("./permission.model");
const Query = {
    permissions: () => {
        return permission_model_1.PermissionModel.find().lean(true);
    }
};
const Mutation = {
    createPermission: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { name, desc } = args;
        const fetchedPermission = yield permission_model_1.PermissionModel.findOne({ name });
        if (fetchedPermission)
            throw error_helper_1.ErrorHelper.userError(`Tên quyền bị trùng`);
        return yield permission_model_1.PermissionModel.create({ name, desc });
    }),
    deletePermissions: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const results = yield permission_model_1.PermissionModel.deleteMany({
            _id: { $in: args._ids },
            readOnly: false
        });
        args._ids.maps((_id) => permission_model_1.PermissionLoader.clear(_id));
        return results;
    }),
    updatePermission: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { _id, name, desc } = args;
        const permission = yield permission_model_1.PermissionLoader.load(_id);
        if (permission.readOnly)
            throw error_helper_1.ErrorHelper.readOnlyError(permission.name);
        return yield permission.updateOne({ _id }, {
            $set: { name, desc }
        });
    })
};
exports.default = {
    Query,
    Mutation
};
