"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const schema = apollo_server_express_1.gql `
    extend type Query {
        permissions: [Permission]
    }

    extend type Mutation {
        createPermission(name: String!, desc: String): Permission!
        deletePermissions(_ids: [ID!]): Mixed
        updatePermission(_id: ID!, name: String, desc: String): Permission!
    }

    type Permission {
        _id: ID
        name: String
        desc: String
        readOnly: Boolean
    }
`;
exports.default = schema;
