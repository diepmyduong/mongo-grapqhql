"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const error_helper_1 = require("../../../helper/error/error.helper");
const util_helper_1 = require("../../../helper/util/util.helper");
const auth_helper_1 = require("../../helper/auth.helper");
const settingGroup_model_1 = require("../settingGroup/settingGroup.model");
const role_1 = require("../user/role");
const setting_model_1 = require("./setting.model");
const Query = {
    setting: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        const setting = yield setting_model_1.SettingModel.findOne({ key: args.key });
        if (setting && setting.isPrivate)
            auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        return setting;
    })
};
const Mutation = {
    createSetting: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { type, name, key, value, isActive, isPrivate, groupID } = args;
        const fetchedGroup = yield settingGroup_model_1.SettingGroupLoader.load(groupID);
        if (!fetchedGroup)
            throw error_helper_1.ErrorHelper.mgRecoredNotFound();
        const fetchedSetting = yield setting_model_1.SettingModel.findOne({ key });
        if (fetchedSetting)
            throw error_helper_1.ErrorHelper.duplicateError(key);
        const newSetting = yield setting_model_1.SettingModel.create({
            type,
            name,
            key,
            value,
            isActive,
            isPrivate,
            group: groupID
        });
        yield fetchedGroup.update({ $push: { settings: newSetting._id } }).exec();
        settingGroup_model_1.SettingGroupLoader.clear(fetchedGroup.id);
        return newSetting;
    }),
    updateSetting: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const { _id, name, key, value, isActive, isPrivate } = args;
        const fetchedSetting = yield setting_model_1.SettingLoader.load(_id);
        if (!fetchedSetting)
            throw error_helper_1.ErrorHelper.mgRecoredNotFound();
        if (fetchedSetting.readOnly)
            throw error_helper_1.ErrorHelper.readOnlyError(fetchedSetting.name);
        const newKeySetting = yield setting_model_1.SettingModel.findOne({ key });
        if (newKeySetting && newKeySetting.id != fetchedSetting.id) {
            throw error_helper_1.ErrorHelper.duplicateError(key);
        }
        yield fetchedSetting
            .update(util_helper_1.UtilHelper.compactObject({ name, key, value, isActive, isPrivate }), {
            new: true
        })
            .exec();
        setting_model_1.SettingLoader.clear(fetchedSetting.id);
        return setting_model_1.SettingLoader.load(fetchedSetting.id);
    }),
    deleteSettings: (root, args, context) => __awaiter(void 0, void 0, void 0, function* () {
        auth_helper_1.AuthHelper.checkRoles(context, role_1.ROLES_ADMIN);
        const removeSettings = yield setting_model_1.SettingLoader.loadMany(args._ids);
        const validSettings = [];
        const groupBulk = settingGroup_model_1.SettingGroupModel.collection.initializeUnorderedBulkOp();
        for (const setting of removeSettings) {
            if (!setting.readOnly) {
                groupBulk
                    .find({ _id: setting.group })
                    .updateOne({ $pull: { settings: setting._id } });
                settingGroup_model_1.SettingGroupLoader.clear(setting.group.toString());
                validSettings.push(setting);
            }
        }
        const removedSettings = yield setting_model_1.SettingModel.deleteMany({
            _id: { $in: validSettings.map(s => s._id) }
        });
        yield groupBulk.execute();
        setting_model_1.SettingLoader.clearAll();
        return removedSettings;
    })
};
exports.default = {
    Query,
    Mutation
};
