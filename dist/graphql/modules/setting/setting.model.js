"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dataloader_1 = __importDefault(require("dataloader"));
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_hidden_1 = __importDefault(require("mongoose-hidden"));
const main_1 = require("../../../config/connections/main");
const Schema = mongoose_1.default.Schema;
const settingSchema = new Schema({
    type: { type: String, required: true, default: 'text' },
    name: { type: String, required: true },
    key: { type: String, required: true },
    value: { type: Schema.Types.Mixed, required: true },
    isActive: { type: Boolean, required: true, default: true },
    isPrivate: { type: Boolean, required: true, default: false },
    readOnly: { type: Boolean, default: false },
    group: { type: Schema.Types.ObjectId, required: true }
}, { timestamps: true });
settingSchema.index({ key: 1 }, { unique: true });
settingSchema.plugin(mongoose_hidden_1.default({ defaultHidden: { _id: false } }));
exports.SettingModel = main_1.MainConnection.model('Setting', settingSchema);
exports.SettingLoader = new dataloader_1.default((ids) => {
    return exports.SettingModel.find({ _id: { $in: ids } }).exec();
});
