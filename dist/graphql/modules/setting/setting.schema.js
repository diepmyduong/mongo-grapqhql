"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const schema = apollo_server_express_1.gql `
    extend type Query {
        setting(key: String!): Setting
    }

    extend type Mutation {
        createSetting(groupID: ID!, type: String!, name: String!, key: String!, value: Mixed!, isActive: Boolean, isPrivate: Boolean): Setting
        updateSetting(_id: ID!, name: String, key: String, value: Mixed, isActive: Boolean, isPrivate: Boolean): Setting
        deleteSettings(_ids: [ID!]): Mixed
    }

    type Setting {
        _id: ID
        type: String
        name: String
        key: String
        value: Mixed
        isActive: Boolean
        isPrivate: Boolean
        readOnly: Boolean
    }
`;
exports.default = schema;
