"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const schema = apollo_server_express_1.gql `
    extend type Query {
        roles: [Role]
    }

    type Role {
        _id: ID
        name: String
        desc: String
        readOnly: Boolean
    }
`;
exports.default = schema;
