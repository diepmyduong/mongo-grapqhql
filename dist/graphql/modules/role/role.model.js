"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dataloader_1 = __importDefault(require("dataloader"));
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_hidden_1 = __importDefault(require("mongoose-hidden"));
const main_1 = require("../../../config/connections/main");
const Schema = mongoose_1.default.Schema;
const roleSchema = new Schema({
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false }
}, { timestamps: true });
roleSchema.index({ name: 1 }, { unique: true });
roleSchema.plugin(mongoose_hidden_1.default({ defaultHidden: { _id: false } }));
exports.RoleModel = main_1.MainConnection.model('Role', roleSchema);
exports.RoleLoader = new dataloader_1.default((ids) => {
    return exports.RoleModel.find({ _id: { $in: ids } }).exec();
});
