"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const role_model_1 = require("./role.model");
const Query = {
    roles: (root, args) => {
        return role_model_1.RoleModel.find().lean(true);
    }
};
const Mutation = {};
exports.default = {
    Query,
    Mutation
};
