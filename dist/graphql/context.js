"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const token_helper_1 = require("../helper/token/token.helper");
const user_model_1 = require("./modules/user/user.model");
function onContext({ req }) {
    return __awaiter(this, void 0, void 0, function* () {
        const token = req.headers['x-token'] || req.query['x-token'];
        const context = { isAuth: false };
        if (token) {
            const decodedToken = token_helper_1.TokenHelper.decodeToken(token);
            context.isAuth = true;
            context.user = yield user_model_1.UserLoader.load(decodedToken.userId);
        }
        return context;
    });
}
exports.onContext = onContext;
