"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_express_1 = require("apollo-server-express");
const lodash_1 = __importDefault(require("lodash"));
const path_1 = __importDefault(require("path"));
const util_helper_1 = require("../helper/util/util.helper");
const context_1 = require("./context");
const config_1 = require("../config");
exports.default = (app) => {
    const typeDefs = [
        apollo_server_express_1.gql `
      scalar Mixed

      type Query {
        _empty: String
      }
      type Mutation {
        _empty: String
      }
      input QueryInput {
        limit: Int
        skip: Int
        sort: Mixed
        filter: Mixed
      }
    `
    ];
    let resolvers = {};
    const ModuleFiles = util_helper_1.UtilHelper.walkSyncFiles(path_1.default.join(__dirname, 'modules'));
    ModuleFiles.filter(f => /(.*).schema.js$/.test(f)).map(f => {
        const { default: schema } = require(f);
        typeDefs.push(schema);
    });
    ModuleFiles.filter(f => /(.*).resolver.js$/.test(f)).map(f => {
        const { default: resolver } = require(f);
        resolvers = lodash_1.default.merge(resolvers, resolver);
    });
    const server = new apollo_server_express_1.ApolloServer({
        typeDefs,
        resolvers,
        playground: true,
        introspection: true,
        context: context_1.onContext,
        debug: config_1.config.debug
    });
    server.applyMiddleware({ app });
    console.log('Running Apollo Server on Path', `${config_1.config.domain}${server.graphqlPath}`);
};
