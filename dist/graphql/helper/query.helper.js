"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applyQueryInput = (queryInput = {}, query) => {
    const limit = queryInput.limit || 20;
    const skip = queryInput.skip || 0;
    const sort = queryInput.sort;
    const filter = queryInput.filter;
    query.limit(limit).skip(skip).lean(true);
    if (sort) {
        query.sort(sort);
    }
    if (filter) {
        query.and([filter]);
    }
    return query;
};
