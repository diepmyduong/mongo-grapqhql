"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const error_helper_1 = require("../../helper/error/error.helper");
class AuthHelper {
    static checkRoles(context, roles, throwError = true) {
        if (!context.isAuth) {
            if (!throwError)
                return false;
            throw error_helper_1.ErrorHelper.unauthorized();
        }
        if (!roles.includes(context.user.role)) {
            if (!throwError)
                return false;
            throw error_helper_1.ErrorHelper.permissionDeny();
        }
        return true;
    }
    static checkPermissions(context, permissions, throwError = true) {
        if (!context.isAuth) {
            if (!throwError)
                return false;
            throw error_helper_1.ErrorHelper.unauthorized();
        }
        for (const p of context.user.permissions) {
            if (permissions.includes(p)) {
                return true;
            }
        }
        if (!throwError)
            return false;
        throw error_helper_1.ErrorHelper.permissionDeny();
    }
}
exports.AuthHelper = AuthHelper;
