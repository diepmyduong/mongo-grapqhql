"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const moment_timezone_1 = __importDefault(require("moment-timezone"));
const development_1 = require("./development");
const production_1 = require("./production");
dotenv_1.default.config();
const developmentConfig = new development_1.DevelopmentConfig();
const productionConfig = new production_1.ProductionConfig();
function getConfig(environment) {
    if (environment === 'development') {
        return developmentConfig;
    }
    else if (environment === 'production') {
        return productionConfig;
    }
    else {
        return developmentConfig;
    }
}
exports.config = getConfig(process.env.NODE_ENV || 'development');
console.log('set default timezone', exports.config.timezone);
moment_timezone_1.default.tz.setDefault(exports.config.timezone);
