"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const default_1 = require("./default");
class ProductionConfig extends default_1.DefaultConfig {
    constructor() {
        super(...arguments);
        this.debug = false;
    }
}
exports.ProductionConfig = ProductionConfig;
