"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const __1 = require("../");
const connect = mongoose_1.default.createConnection(__1.config.mgConnection, {
    readPreference: 'secondaryPreferred',
    useNewUrlParser: true,
    socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30,
    useFindAndModify: false,
    useCreateIndex: true
});
exports.MainConnection = connect;
