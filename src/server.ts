import bodyParser from 'body-parser';
import compression from 'compression';
import cookies from 'cookie-parser';
import express from 'express';
import fs from 'fs';
import logger from 'morgan';
import path from 'path';

import { config } from './config';
import grapqhQLServer from './graphql';

console.log(
  'Starting server with at ' + process.pid + ' on port ' + config.port
);
/**
 * Express configuration.
 */
const app = express();
app.use(
  logger('common', {
    skip: function(req: any, res: any) {
      if (req.url == '/_ah/health') {
        return true;
      } else {
        return false;
      }
    }
  })
);
app.use(
  bodyParser.json({
    limit: '50mb'
  })
);
app.use(
  bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
  })
);

// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(compression());
app.use(express.static(path.join(__dirname, './public')));
app.set('views', path.join(__dirname, './templates'));

app.use(cookies());
app.set('port', config.port);
app.get('/api/version', function(request, response) {
  const version = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, 'public/version.json'), 'utf8')
  );
  response.send(`${config.name}: ${version.echo}`);
  response.end();
});
grapqhQLServer(app);

app.listen(app.get('port'), function() {
  console.log(`${config.name} started at ${config.domain}`);
});
