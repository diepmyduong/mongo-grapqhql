export class DefaultConfig {
  name = 'Mongo Node Core';
  protocol = 'http';
  host = 'localhost';
  port = process.env['PORT'] || '5050';
  secret = process.env['SECRET'] || 'hellokitty';
  domain = 'http://localhost:5050';
  timezone = 'Asia/Ho_Chi_Minh';
  debug = true;

  // Adapter
  pageSize = 20;

  // MongoDB
  mgConnection =
    process.env['MG_CONNECTION'] ||
    'mongodb+srv://user:pass@localhost/maindb?retryWrites=true';

  // Firebase Service Account
  firebaseCredential = {
    private_key: process.env.FIREBASE_PRIVATE_KEY
      ? process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n')
      : '',
    type: 'service_account',
    project_id: 'postol',
    private_key_id: '77d570eebe474e4569ce0ce1c16443defd8cabb7',
    client_email: 'firebase-adminsdk-itctc@postol.iam.gserviceaccount.com',
    client_id: '109270553907865385270',
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://oauth2.googleapis.com/token',
    auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
    client_x509_cert_url:
      'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-itctc%40postol.iam.gserviceaccount.com'
  };
  enableRealtimeConfig = false;
}
