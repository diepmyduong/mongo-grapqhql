import mongoose from 'mongoose';
import { config } from '../';

const connect = mongoose.createConnection(config.mgConnection, {
  readPreference: 'secondaryPreferred',
  useNewUrlParser: true,
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30,
  useFindAndModify: false,
  useCreateIndex: true
});

export const MainConnection = connect;
