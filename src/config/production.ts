import { DefaultConfig } from './default';

export class ProductionConfig extends DefaultConfig {
  debug = false;
}
