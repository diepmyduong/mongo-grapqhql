import { DefaultConfig } from './default';

export class DevelopmentConfig extends DefaultConfig {
    debug = false;
}
