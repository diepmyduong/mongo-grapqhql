import { ErrorHelper } from "../../helper/error/error.helper";
import { Context } from "../context";

export class AuthHelper {
  static acceptRoles(context: Context, roles: String[], throwError = true) {
    this.checkValidAuth(context, throwError);
    if (!roles.includes(context.user!.role!)) {
      if (!throwError) return false;
      throw ErrorHelper.permissionDeny();
    }
    return true;
  }
  static denyRoles(context: Context, roles: string[], throwError = true) {
    this.checkValidAuth(context, throwError);
    if (roles.includes(context.user!.role!)) {
      if (!throwError) return false;
      throw ErrorHelper.permissionDeny();
    }
    return true;
  }
  static acceptPermissions(
    context: Context,
    permissions: String[],
    throwError = true
  ) {
    this.checkValidAuth(context, throwError);
    for (const p of context.user!.permissions!) {
      if (permissions.includes(p)) {
        return true;
      }
    }
    if (!throwError) return false;
    throw ErrorHelper.permissionDeny();
  }
  static denyPermissions(
    context: Context,
    permissions: String[],
    throwError = true
  ) {
    if (!this.checkValidAuth(context)) {
      return true;
    }
    for (const p of context.user!.permissions!) {
      if (permissions.includes(p)) {
        return true;
      }
    }
    if (!throwError) return false;
    throw ErrorHelper.permissionDeny();
  }
  static checkValidAuth(context: Context, throwError = true) {
    if (context.tokenExpired) {
      if (!throwError) return false;
      throw ErrorHelper.unauthorized();
    }
    if (!context.isAuth) {
      if (!throwError) return false;
      throw ErrorHelper.unauthorized();
    }
    return true;
  }
  static fieldAccept(roles?: string[], permissions?: string[]) {
    return (root: any, args: any, context: Context, { fieldName }: any) => {
      if (roles) this.acceptRoles(context, roles);
      if (permissions) this.acceptPermissions(context, permissions);
      return root[fieldName];
    };
  }
  static fieldDeny(roles?: string[], permissions?: string[]) {
    return (root: any, args: any, context: Context, { fieldName }: any) => {
      this.checkValidAuth(context);
      if (roles) this.acceptRoles(context, roles);
      if (permissions) this.acceptPermissions(context, permissions);
      return root[fieldName];
    };
  }
}
