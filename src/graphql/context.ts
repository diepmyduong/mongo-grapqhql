import { Request } from 'express';

import { TokenHelper } from '../helper/token/token.helper';
import { UserDoc, UserLoader } from './modules/user/user.model';

export type Context = {
  isAuth: boolean;
  user?: UserDoc;
  tokenExpired: boolean;
};

export async function onContext({ req }: { req: Request }) {
  // get the user token from the headers
  const token = req.headers['x-token'] || req.query['x-token'];
  const context: Context = { isAuth: false, tokenExpired: false };
  if (token) {
    const decodedToken = TokenHelper.decodeToken(token);
    if (!decodedToken) {
      context.isAuth = false;
    } else if (new Date(decodedToken.exp).getTime() <= Date.now()) {
      context.tokenExpired = true;
      context.isAuth = false;
    } else {
      context.isAuth = true;
      context.user = await UserLoader.load(decodedToken.userId);
    }
  }
  return context;
}
