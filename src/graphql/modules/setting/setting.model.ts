import DataLoader from 'dataloader';
import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { MainConnection } from '../../../config/connections/main';
import { BaseDocument } from '../model';

const Schema = mongoose.Schema;

export type SettingDoc = BaseDocument & {
  type?: string;
  name?: string;
  key?: string;
  value?: any;
  isActive?: boolean;
  isPrivate?: boolean;
  readOnly?: boolean;
  group?: string;
};

const settingSchema = new Schema(
  {
    type: { type: String, required: true, default: 'text' },
    name: { type: String, required: true },
    key: { type: String, required: true },
    value: { type: Schema.Types.Mixed, required: true },
    isActive: { type: Boolean, required: true, default: true },
    isPrivate: { type: Boolean, required: true, default: false },
    readOnly: { type: Boolean, default: false },
    group: { type: Schema.Types.ObjectId, required: true }
  },
  { timestamps: true }
);

settingSchema.index({ key: 1 }, { unique: true });
settingSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let SettingModel: mongoose.Model<SettingDoc> = MainConnection.model(
  'Setting',
  settingSchema
);

export const SettingLoader = new DataLoader<string, SettingDoc>(
  (ids: string[]) => {
    return SettingModel.find({ _id: { $in: ids } }).exec();
  }
);
