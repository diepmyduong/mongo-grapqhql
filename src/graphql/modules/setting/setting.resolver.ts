import { ErrorHelper } from '../../../helper/error/error.helper';
import { UtilHelper } from '../../../helper/util/util.helper';
import { Context } from '../../context';
import { AuthHelper } from '../../helper/auth.helper';
import {
  SettingGroupLoader,
  SettingGroupModel
} from '../settingGroup/settingGroup.model';
import { ROLES_ADMIN } from '../user/role';
import { SettingLoader, SettingModel } from './setting.model';

const Query = {
  setting: async (root: any, args: any, context: Context) => {
    const setting = await SettingModel.findOne({ key: args.key });
    if (setting && setting.isPrivate)
      AuthHelper.checkRoles(context, ROLES_ADMIN);
    return setting;
  }
};

const Mutation = {
  createSetting: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { type, name, key, value, isActive, isPrivate, groupID } = args;

    const fetchedGroup = await SettingGroupLoader.load(groupID);
    if (!fetchedGroup) throw ErrorHelper.mgRecoredNotFound();
    const fetchedSetting = await SettingModel.findOne({ key });
    if (fetchedSetting) throw ErrorHelper.duplicateError(key);

    const newSetting = await SettingModel.create({
      type,
      name,
      key,
      value,
      isActive,
      isPrivate,
      group: groupID
    });
    await fetchedGroup.update({ $push: { settings: newSetting._id } }).exec();
    SettingGroupLoader.clear(fetchedGroup.id);
    return newSetting;
  },
  updateSetting: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { _id, name, key, value, isActive, isPrivate } = args;

    const fetchedSetting = await SettingLoader.load(_id);
    if (!fetchedSetting) throw ErrorHelper.mgRecoredNotFound();
    if (fetchedSetting.readOnly)
      throw ErrorHelper.readOnlyError(fetchedSetting.name!);
    const newKeySetting = await SettingModel.findOne({ key });
    if (newKeySetting && newKeySetting.id != fetchedSetting.id) {
      throw ErrorHelper.duplicateError(key);
    }
    await fetchedSetting
      .update(
        UtilHelper.compactObject({ name, key, value, isActive, isPrivate }),
        {
          new: true
        }
      )
      .exec();
    SettingLoader.clear(fetchedSetting.id);
    return SettingLoader.load(fetchedSetting.id);
  },
  deleteSettings: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const removeSettings = await SettingLoader.loadMany(args._ids);
    const validSettings = [];
    const groupBulk = SettingGroupModel.collection.initializeUnorderedBulkOp();
    for (const setting of removeSettings) {
      if (!setting.readOnly) {
        groupBulk
          .find({ _id: setting.group })
          .updateOne({ $pull: { settings: setting._id } });
        SettingGroupLoader.clear(setting.group!.toString());
        validSettings.push(setting);
      }
    }
    const removedSettings = await SettingModel.deleteMany({
      _id: { $in: validSettings.map(s => s._id) }
    });
    await groupBulk.execute();
    SettingLoader.clearAll();
    return removedSettings;
  }
};

export default {
  Query,
  Mutation
};
