import { gql } from 'apollo-server-express';

const schema = gql`
    extend type Query {
        settingGroups: [SettingGroup]
        settingGroup(name: String!): SettingGroup
    }

    extend type Mutation {
        createSettingGroup(name: String!, desc: String): SettingGroup
        updateSettingGroup(_id: ID!, name: String, desc: String): SettingGroup
        deleteSettingGroups(_ids: [ID!]): Mixed
    }

    type SettingGroup {
        _id: ID
        name: String
        desc: String
        readOnly: Boolean
        settings: [Setting]
    }
`

export default schema;