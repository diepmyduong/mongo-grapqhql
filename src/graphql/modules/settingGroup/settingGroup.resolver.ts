import { ErrorHelper } from '../../../helper/error/error.helper';
import { Context } from '../../context';
import { AuthHelper } from '../../helper/auth.helper';
import { SettingLoader } from '../setting/setting.model';
import { ROLES_ADMIN } from '../user/role';
import {
  SettingGroupDoc,
  SettingGroupLoader,
  SettingGroupModel
} from './settingGroup.model';

const Query = {
  settingGroups: (root: any, args: any, context: Context) => {
    return SettingGroupModel.find()
      .lean(true)
      .exec();
  },
  settingGroup: (root: any, args: any, context: Context) => {
    return SettingGroupModel.findOne({ name: args.name })
      .lean(true)
      .exec();
  }
};

const Mutation = {
  createSettingGroup: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { name, desc } = args;
    const fetchedGroup = await SettingGroupModel.findOne({ name });
    if (fetchedGroup) throw ErrorHelper.duplicateError(name);
    const newGroup = await SettingGroupModel.create({ name, desc });
    SettingGroupLoader.prime(newGroup.id, newGroup);
    return newGroup;
  },
  updateSettingGroup: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { _id, name, desc } = args;

    const fetchedGroup = await SettingGroupLoader.load(_id);
    if (!fetchedGroup) throw ErrorHelper.mgRecoredNotFound();
    if (fetchedGroup.readOnly)
      throw ErrorHelper.readOnlyError(fetchedGroup.name!);
    await fetchedGroup
      .set('name', name)
      .set('desc', desc)
      .save();
    return fetchedGroup;
  },
  deleteSettingGroups: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const removedGroups = await SettingGroupModel.deleteMany({
      _id: { $in: args._ids },
      readOnly: false
    });
    SettingGroupLoader.clearAll();
    return removedGroups;
  }
};

const SettingGroup = {
  settings: async (root: SettingGroupDoc, args: any, context: Context) => {
    return SettingLoader.loadMany(root.settings!).then(settings => {
      return settings.filter(s => {
        return (
          !s.isPrivate || AuthHelper.checkRoles(context, ROLES_ADMIN, false)
        );
      });
    });
  }
};

export default {
  Query,
  Mutation,
  SettingGroup
};
