import DataLoader from 'dataloader';
import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { MainConnection } from '../../../config/connections/main';
import { BaseDocument } from '../model';

const Schema = mongoose.Schema;

export type SettingGroupDoc = BaseDocument & {
  name?: string;
  desc?: string;
  readOnly?: boolean;
  settings?: string[];
};

const settingGroupSchema = new Schema(
  {
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false },
    settings: { type: [Schema.Types.ObjectId], default: [] }
  },
  { timestamps: true }
);

settingGroupSchema.index({ name: 1 }, { unique: true });
settingGroupSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let SettingGroupModel: mongoose.Model<
  SettingGroupDoc
> = MainConnection.model('SettingGroup', settingGroupSchema);

export const SettingGroupLoader = new DataLoader<string, SettingGroupDoc>(
  (ids: string[]) => {
    return SettingGroupModel.find({ _id: { $in: ids } }).exec();
  }
);
