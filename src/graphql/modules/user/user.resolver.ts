import bcrypt from 'bcryptjs';

import { ErrorHelper } from '../../../helper/error/error.helper';
import { TokenHelper } from '../../../helper/token/token.helper';
import { Context } from '../../context';
import { AuthHelper } from '../../helper/auth.helper';
import { applyQueryInput } from '../../helper/query.helper';
import {
  ROLES_ADMIN,
  ROLES_ADMIN_EDITOR,
  ROLES_ADMIN_EDITOR_CUSTOMER
} from './role';
import { UserModel } from './user.model';

const Query = {
  users: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN_EDITOR);
    const query = UserModel.find().lean(true);
    return applyQueryInput(args.q, query);
  }
};

const Mutation = {
  createUser: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { username, password, role, permissions } = args;
    if (!ROLES_ADMIN_EDITOR_CUSTOMER.includes(role)) {
      throw ErrorHelper.userError(
        `Vai trò không hợp lệ. Chỉ hổ trợ ${ROLES_ADMIN_EDITOR_CUSTOMER.join(
          ','
        )}`
      );
    }
    const fetchedUser = await UserModel.findOne({ username });
    if (fetchedUser) throw ErrorHelper.userExisted();
    const newUser = await UserModel.create({
      username: username,
      password: await bcrypt.hash(password, 12),
      role: role,
      permissions
    });
    return newUser.toJSON();
  },
  login: async (root: any, args: any) => {
    const { username, password } = args;
    const user = await UserModel.findOne({ username });
    if (!user) {
      throw ErrorHelper.userNotExist();
    }
    const isEqual = await bcrypt.compare(password, user.password!);
    if (!isEqual) {
      throw new Error('Password is incorrect!');
    }

    const token = TokenHelper.generateToken({
      userId: user._id,
      username: user.username,
      role: user.role,
      permissions: user.permissions
    });
    return { token, user: user.toJSON() };
  }
};

export default {
  Query,
  Mutation,
  User: {
    password: AuthHelper.authField(ROLES_ADMIN)
  }
};
