import DataLoader from 'dataloader';
import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { MainConnection } from '../../../config/connections/main';
import { BaseDocument } from '../model';

const Schema = mongoose.Schema;

export type UserDoc = BaseDocument & {
  username?: string;
  password?: string;
  role?: string;
  permissions?: string[];
};

const userSchema = new Schema(
  {
    username: { type: String, required: true },
    password: { type: String, required: true, hideJSON: true },
    role: { type: String, required: true },
    permissions: { type: [String], default: [] }
  },
  { timestamps: true }
);

userSchema.index({ username: 1 }, { unique: true });
userSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let UserModel: mongoose.Model<UserDoc> = MainConnection.model(
  'User',
  userSchema
);

export const UserLoader = new DataLoader<string, UserDoc>((ids: string[]) => {
  return UserModel.find({ _id: { $in: ids } }).exec();
});
