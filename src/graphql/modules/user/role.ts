export const ROLE_ADMIN = 'admin';
export const ROLE_EDITOR = 'editor';
export const ROLE_CUSTOMER = 'customer';

export const ROLES_ADMIN = ['admin'];
export const ROLES_ADMIN_EDITOR = ['admin', 'editor'];
export const ROLES_ADMIN_EDITOR_CUSTOMER = ['admin', 'editor', 'customer'];
