import { gql } from 'apollo-server-express';

const schema = gql`
    extend type Mutation {
        createUser(username: String!, password: String!, role: String!, permissions: [String]): User!
        login(username: String!, password: String!): AuthData!
    }
    
    extend type Query {
        users(q: QueryInput): [User]
    }

    type AuthData {
        user: User!
        token: String!
    }

    type User {
        _id: ID
        username: String
        role: String
        permissions: [String]
        password: String
    }
`

export default schema;