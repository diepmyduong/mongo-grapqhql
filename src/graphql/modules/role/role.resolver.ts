import { RoleModel } from "./role.model";

const Query = {
  roles: (root: any, args: any) => {
    return RoleModel.find().lean(true);
  }
};

const Mutation = {
  
};

export default {
  Query,
  Mutation
};
