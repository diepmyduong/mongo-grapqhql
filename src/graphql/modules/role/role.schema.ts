import { gql } from 'apollo-server-express';

const schema = gql`
    extend type Query {
        roles: [Role]
    }

    type Role {
        _id: ID
        name: String
        desc: String
        readOnly: Boolean
    }
`

export default schema;