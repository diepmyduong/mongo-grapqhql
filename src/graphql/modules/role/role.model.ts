import DataLoader from 'dataloader';
import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { MainConnection } from '../../../config/connections/main';
import { BaseDocument } from '../model';

const Schema = mongoose.Schema;

export type RoleDoc = BaseDocument & {
  name?: string;
  desc?: string;
  readOnly?: boolean;
};

const roleSchema = new Schema(
  {
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false }
  },
  { timestamps: true }
);

roleSchema.index({ name: 1 }, { unique: true });
roleSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let RoleModel: mongoose.Model<RoleDoc> = MainConnection.model(
  'Role',
  roleSchema
);

export const RoleLoader = new DataLoader<string, RoleDoc>((ids: string[]) => {
  return RoleModel.find({ _id: { $in: ids } }).exec();
});
