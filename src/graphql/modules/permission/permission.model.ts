import DataLoader from 'dataloader';
import mongoose from 'mongoose';
import mongooseHidden from 'mongoose-hidden';

import { MainConnection } from '../../../config/connections/main';
import { BaseDocument } from '../model';

const Schema = mongoose.Schema;

export type PermissionDoc = BaseDocument & {
  name?: string;
  desc?: string;
  readOnly?: boolean;
};

const permissionSchema = new Schema(
  {
    name: { type: String, required: true },
    desc: { type: String },
    readOnly: { type: Boolean, default: false }
  },
  { timestamps: true }
);

permissionSchema.index({ name: 1 }, { unique: true });
permissionSchema.plugin(mongooseHidden({ defaultHidden: { _id: false } }));

export let PermissionModel: mongoose.Model<
  PermissionDoc
> = MainConnection.model('Permission', permissionSchema);

export const PermissionLoader = new DataLoader<string, PermissionDoc>(
  (ids: string[]) => {
    return PermissionModel.find({ _id: { $in: ids } }).exec();
  }
);
