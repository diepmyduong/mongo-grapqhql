import { ErrorHelper } from '../../../helper/error/error.helper';
import { Context } from '../../context';
import { AuthHelper } from '../../helper/auth.helper';
import { ROLES_ADMIN } from '../user/role';
import { PermissionLoader, PermissionModel } from './permission.model';

const Query = {
  permissions: () => {
    return PermissionModel.find().lean(true);
  }
};

const Mutation = {
  createPermission: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { name, desc } = args;
    const fetchedPermission = await PermissionModel.findOne({ name });
    if (fetchedPermission) throw ErrorHelper.userError(`Tên quyền bị trùng`);
    return await PermissionModel.create({ name, desc });
  },
  deletePermissions: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const results = await PermissionModel.deleteMany({
      _id: { $in: args._ids },
      readOnly: false
    });
    args._ids.maps((_id: string) => PermissionLoader.clear(_id));
    return results;
  },
  updatePermission: async (root: any, args: any, context: Context) => {
    AuthHelper.checkRoles(context, ROLES_ADMIN);
    const { _id, name, desc } = args;
    const permission = await PermissionLoader.load(_id);
    if (permission.readOnly) throw ErrorHelper.readOnlyError(permission.name!);
    return await permission.updateOne(
      { _id },
      {
        $set: { name, desc }
      }
    );
  }
};

export default {
  Query,
  Mutation
};
