import { gql } from 'apollo-server-express';

const schema = gql`
    extend type Query {
        permissions: [Permission]
    }

    extend type Mutation {
        createPermission(name: String!, desc: String): Permission!
        deletePermissions(_ids: [ID!]): Mixed
        updatePermission(_id: ID!, name: String, desc: String): Permission!
    }

    type Permission {
        _id: ID
        name: String
        desc: String
        readOnly: Boolean
    }
`

export default schema;