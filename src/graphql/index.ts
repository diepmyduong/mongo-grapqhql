import { ApolloServer, gql } from 'apollo-server-express';
import { Express } from 'express';
import _ from 'lodash';
import path from 'path';

import { UtilHelper } from '../helper/util/util.helper';
import { onContext } from './context';
import { config } from '../config';

export default (app: Express) => {
  const typeDefs = [
    gql`
      scalar Mixed

      type Query {
        _empty: String
      }
      type Mutation {
        _empty: String
      }
      input QueryInput {
        limit: Int
        skip: Int
        sort: Mixed
        filter: Mixed
      }
    `
  ];

  let resolvers = {};

  const ModuleFiles = UtilHelper.walkSyncFiles(path.join(__dirname, 'modules'));
  ModuleFiles.filter(f => /(.*).schema.js$/.test(f)).map(f => {
    const { default: schema } = require(f);
    typeDefs.push(schema);
  });
  ModuleFiles.filter(f => /(.*).resolver.js$/.test(f)).map(f => {
    const { default: resolver } = require(f);
    resolvers = _.merge(resolvers, resolver);
  });
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    playground: true,
    introspection: true,
    context: onContext,
    debug: config.debug
  });

  server.applyMiddleware({ app });

  console.log('Running Apollo Server on Path', `${config.domain}${server.graphqlPath}`);
};
