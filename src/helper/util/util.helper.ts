import Ajv from 'ajv';
import AjvError from 'ajv-errors';
import AjvKeyWords from 'ajv-keywords';
import crypto from 'crypto';
import fs from 'fs';
import _ from 'lodash';
import path from 'path';

export class UtilHelper {
  static validateJSON(schema: any, json: any = {}) {
    const ajv = new Ajv({ allErrors: true, jsonPointers: true });
    AjvError(ajv, { singleError: true });
    AjvKeyWords(ajv, ['switch']);
    const valid = ajv.validate(schema, json);
    return {
      isValid: valid,
      message: ajv.errorsText()
    };
  }
  static booleanString(condition: any) {
    return condition ? 'True' : 'False';
  }
  static hash(data: any) {
    let s = data;
    if (!_.isString(data)) {
      s = JSON.stringify(data);
    }
    return crypto
      .createHash('md5')
      .update(s)
      .digest('hex');
  }
  static walkSyncFiles(dir: string, filelist: string[] = []) {
    const files = fs.readdirSync(dir);
    files.forEach(function(file) {
      if (fs.statSync(path.join(dir, file)).isDirectory()) {
        filelist = UtilHelper.walkSyncFiles(path.join(dir, file), filelist);
      } else {
        filelist.push(path.join(dir, file));
      }
    });
    return filelist;
  }
  static compactObject(obj: any) {
    return _.omitBy(obj, _.isUndefined);
  }
}
