import jwt from 'jwt-simple';
import moment from 'moment';

import { config } from '../../config';

export interface IGenerateTokenOption {
  exp?: moment.Moment;
  secret?: string;
}
export interface IDecodeTokenOption {
  secret?: string;
}
export class TokenHelper {
  static generateToken(
    payload: any,
    option: IGenerateTokenOption = {}
  ): string {
    const secret = option.secret || config.secret;
    return jwt.encode(
      {
        payload: payload,
        exp: option.exp || moment().add(1, 'days')
      },
      secret
    );
  }
  static decodeToken(token: string, option: IDecodeTokenOption = {}) {
    let result = undefined;
    try {
      const secret = option.secret || config.secret;
      result = jwt.decode(token, secret);
      if (result) {
        return result.payload;
      } else {
        return;
      }
    } catch (err) {
      return;
    }
  }
}
