FROM node:10

MAINTAINER Dương Jerry <diepmyduong@gmail.com>

ADD package.json /tmp/package.json
# RUN npm install -g typescript@2.9.*
RUN cd /tmp && npm install
RUN mkdir -p /usr/src/app && cp -a /tmp/node_modules /usr/src/app
# Create app directory
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN cd /usr/src/app
EXPOSE 5050
CMD [ "npm", "run", "start" ]