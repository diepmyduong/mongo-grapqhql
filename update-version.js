var fs = require('fs');
var path = require('path');

const file = path.resolve('src/public/version.json');
const json = JSON.parse(fs.readFileSync(file));

json.fixbug++;
json.timestamp = new Date();
json.echo = `v${json.version}.${json.feature}.${json.fixbug} - ${json.timestamp.toString()}`;
json.log.push(json.echo);
console.log(json.echo);
fs.writeFileSync(file, JSON.stringify(json, null, 2));